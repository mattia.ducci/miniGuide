note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	make

feature	--inizializzation
	make

	local
			i:INTEGER
		do
			create rolls.make_empty
			cnt_rolls:=1

			from
				i:=1
			until
				i>21
			loop
				rolls.force (0, i)
				i:=i+1
			end
		ensure
			empty_match: rolls.count = 21 and score = 0
		end


feature	--status
	rolls: ARRAY[INTEGER]

	cnt_rolls:INTEGER

	sum_of_rolls:INTEGER

		require
			not_empty_array: not rolls.is_empty
		local
			sum: INTEGER
		do
			across rolls
				 as roll
			loop
				sum := sum+roll.item.as_integer_32
			end
		end

feature --settings


	add_roll(n:INTEGER)
		--aggiunge punteggio di un tiro all'array
		require
			adding_roll_exist: n >= 0 and n <= 10

		do
			rolls.force(n,cnt_rolls)
			cnt_rolls:=cnt_rolls+1
		ensure
			one_more_roll: cnt_rolls = old cnt_rolls+1
			added_roll: rolls[cnt_rolls-1] = n
		end

feature --check

	is_strike(cur_roll:INTEGER):BOOLEAN
		--controlla se il frame incominciato da cur_roll � un frame di strike (valore cur_roll=10)
		require
			adding_roll_exist: cur_roll >= 0 and cur_roll <= 21
		do
			Result := rolls[cur_roll] = 10
		ensure
			strike_throw: rolls[cur_roll] = 10 implies Result = TRUE
		end


	is_spare(cur_roll: INTEGER):BOOLEAN
		require
			adding_roll_exist: cur_roll >= 0 and cur_roll <= 21
		do
			Result := frame_points(cur_roll) = 10
		ensure
			spare_throw : frame_points(cur_roll) = 10 implies Result = TRUE
		end


feature --punteggio

	frame_points(cur_roll:INTEGER):INTEGER
		require
			adding_roll_exist: cur_roll >= 0 and cur_roll <= 20
		do
			--print("cur roll: "+rolls[cur_roll])
			Result := rolls[cur_roll] + rolls[cur_roll+1]
		ensure
			frame_points_valid: Result >=0 and Result <= 20
		end

	strike_bonus(cur_roll: INTEGER):INTEGER
		require
			adding_roll_exist: cur_roll >= 0 and cur_roll <= 19
		do
			Result := frame_points(cur_roll+1)
		ensure
			strike_bonus: Result = frame_points(cur_roll+1)
		end


	spare_bonus(cur_roll: INTEGER):INTEGER
		require
			adding_roll_exist: cur_roll >= 0 and cur_roll <= 21
		do
			Result := rolls[cur_roll+2]
		ensure
			Result = rolls[cur_roll+2]
		end

	score:INTEGER
		local
			s:INTEGER
			frame:INTEGER
			i:INTEGER --fino a 19 x strike, 20 x spare
		do
			from
				frame:=1
				i:=1
			until
				frame>10
			loop
				if is_strike(i) then

					s := s+10+strike_bonus(i)-- all'ultimo frame strike bonus sono i due tiri
					i:=i+1

				elseif is_spare(i) then

					s := s+10+spare_bonus(i) -- se � uno spare per forza la somma di i+(i+1)=10, x questo faccio i+2
					i:=i+2

				else --tiro normale

					s := s+frame_points(i)
					i:=i+2

				end
				frame:=frame+1
			end
			Result:=s
		end

invariant
	valid_throw: cnt_rolls >= 0 and cnt_rolls <= 21
	valid_score: score >= 0 and score <= 300

end
