note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	NEW_TEST_SET

inherit
	EQA_TEST_SET
		redefine
			on_prepare
		end

feature {NONE} -- Events

	g:GAME

	on_prepare
			-- <Precursor>
		do
			--assert ("not_implemented", False)
			create g.make
		end

feature -- Test routines

--	new_test_routine
--			-- New test routine
--		do
--			assert ("not_implemented", False)
--		end

	test_gutter_game
		--test zero punti
		do
			across 1 |..| 20 as i
			loop
				g.add_roll (0)
				print(i.out)
			end
			assert("Gutter game ok",g.score =0)
		end

	test_all_ones
		do
			across 1 |..| 20 as i
			loop
				g.add_roll (1)
				print(i.out)
			end
			assert("Gutter game ok",g.score =20)

		end

	test_one_spare
		do
			g.add_roll (5)
			g.add_roll (5)
			g.add_roll (3)
			assert("one spare",g.score=16)
		end

	test_one_strike
		do
			g.add_roll (10)
			g.add_roll (4)
			g.add_roll (1)
			assert("one strike",g.score=20)
		end

	test_perfect_game
		do
			across 1 |..| 12 as i loop

				g.add_roll (10)
			end
			assert("perfect game",g.score=300)
		end

	test_last_spare
		do
			across 1|..| 9 as i loop

				g.add_roll (10)
			end
			g.add_roll (5)
			g.add_roll (5)
			g.add_roll (10)
			print("%Nscore del test last spare: "+g.score.out)
			assert("test last spare",g.score=275)
		end



end


