note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	make

feature {NONE} -- Initialization

	make
			-- Initialization for `Current'.
			local
				i:INTEGER
		do
			create frames.make_empty--init a array vuoto
			num_frame:=1
			from
				i:=1
			until
				i>9
			loop
				frames.force (create {ORDINARY_FRAME}.make, i)	--primi 9 frame ordinari
				i:=i+1 --ricordarsi di implementarla!!!
			end

			frames.force (create {LAST_FRAME}.make, 10) --mark last as last_frame
			print("game created with "+frames.count.out+" frames%N")

		end

feature --game

	roll(pins:INTEGER)
		--mette  il valore del tiro nel frame giusto
		do
			if frames.item (num_frame).is_full then

				num_frame:=num_frame+1
			end
			--print("num frame: "+num_frame.out+"%N")
			frames.item (num_frame).add_throw(pins)
			print("Al frame "+num_frame.out+" ->  aggiunto il tiro. Frame score: "+frames.item (num_frame).frame_points.out+"%N")
		end

	score:INTEGER
		local
			frame:INTEGER
		do
			Result:=0
			from
				frame:=1
			until
				frame>10
			loop
				if(frames.item (frame).is_strike) then
					Result:=Result+frames.item(frame).frame_points+strike_bonus (frame)

				elseif(frames.item (frame).is_spare) then
					Result:=Result+frames.item (frame).frame_points+spare_bonus (frame)

				else -- non � ne strike ne spare
					Result:=Result+frames.item (frame).frame_points

				end
				frame:=frame+1
			end
		end

	spare_bonus(num_spare_frame:INTEGER):INTEGER
		do
			if num_spare_frame < 10 then --siamo in un frame ordinario
				Result:= frames.item (num_spare_frame+1).first_throw_pins

			else --ultimo frame	
				Result:=frames.item (num_spare_frame).last_throw_pins
			end

		end

	strike_bonus(num_strike_frame:INTEGER):INTEGER
		local
			bonus:INTEGER
		do
			if num_strike_frame<10 then --ordinary frame
				bonus:=frames.item (num_strike_frame+1).first_throw_pins

				if bonus = 10 then --anche nel tiro successivo � stato fatto uno strike

					if num_strike_frame+1 = 10 then --siamo arrivati all'ultimo frame
						bonus:=bonus+frames.item (num_strike_frame+1).second_throw_pins

					else --siamo ancora in un frame ordinario

						bonus:=bonus+frames.item (num_strike_frame+2).first_throw_pins
					end

				else --se nel tiro successivo al primo strike non � stato fatto uno strike

					bonus:=bonus+frames.item (num_strike_frame+1).second_throw_pins
				end
			else --se � stato fatto strike all'ultimo frame

				bonus:=frames.item (num_strike_frame).second_throw_pins+frames.item (num_strike_frame).last_throw_pins
			end

		Result:=bonus
		end

feature --items

	frames:ARRAY[FRAME]
	num_frame:INTEGER --a che frame siamo arrivati

end
