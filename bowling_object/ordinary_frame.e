note
	description: "Summary description for {ORDINARY_FRAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ORDINARY_FRAME
	inherit
		FRAME

create
	make

feature --items

	throws:ARRAY[INTEGER]

feature {NONE} -- Initialization

	make
			-- Initialization for `Current'.
		do
			create throws.make_filled(-1,1,2)--default value -1 per le celle 1 e 2

		end

feature
	is_full:BOOLEAN
		do
			Result:= frame_points=10 or (throws.item (1) /= -1 and throws.item (2) /= -1)
		end

	add_throw(pins:INTEGER)
		local
			i:INTEGER
			insert:BOOLEAN
		do
			from
				i:=1
			until
				i>2
			loop
				if throws.item (i) = -1 and not insert then
					throws.put (pins, i)
					insert:=TRUE
				end
				i:=i+1
			end

		end

	frame_points:INTEGER
		local
			sum:INTEGER
			i:INTEGER
		do
			from
				i:=1
			until
				i>2
			loop
				if throws.item (i) /= -1 then
					sum:=sum+throws.item (i)
				end
				i:=i+1
			end
			Result:=sum
		end

	first_throw_pins:INTEGER
		do
			Result:=throws.item (1)
		end

	is_strike:BOOLEAN
		do
			Result:= throws.item (1) = 10

		end

	is_spare:BOOLEAN
		do
			Result:= frame_points = 10 and not is_strike
		end

	last_throw_pins:INTEGER
		do
			Result:=throws.item (2)
		end

	second_throw_pins:INTEGER
		do
			Result:=last_throw_pins

		end


end
