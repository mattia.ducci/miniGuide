note
	description: "Summary description for {FRAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	FRAME

feature --game

add_throw(pins:INTEGER)
	deferred
	end

feature --check

is_full:BOOLEAN
	deferred
	end

first_throw_pins:INTEGER
	deferred
	end

second_throw_pins:INTEGER
	deferred
	end

last_throw_pins:INTEGER
	deferred
	end

frame_points:INTEGER
	deferred
	end

is_strike:BOOLEAN
	deferred
	end

is_spare:BOOLEAN
	deferred
	end




end
