note
	description: "Summary description for {LAST_FRAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	LAST_FRAME
		inherit
			FRAME

create
	make

feature --items

	throws:ARRAY[INTEGER]

feature {NONE} -- Initialization

	make
			-- Initialization for `Current'.
		do
			create throws.make_filled(-1,1,3)

		end

	is_full:BOOLEAN --quando implemento un metodo deferred (nella superclasse) non devo mettere la clausola redefine
		do

			Result:= throws.item (1) /= -1 and throws.item (2) /= -1 and throws.item (3) /= -1

		end

	add_throw(pins:INTEGER)
		local
			i:INTEGER
			insert:BOOLEAN
		do
			from
				i:=1
			until
				i>3
			loop
				if throws.item (i) = -1 and not insert then
					throws.put (pins, i)
					insert:=TRUE
				end
				i:=i+1
			end

		end

	frame_points:INTEGER
		local
			sum:INTEGER
			i:INTEGER
		do
			from
				i:=1
			until
				i>3
			loop
				if throws.item (i) /= -1 then
					sum:=sum+throws.item (i)
				end
				i:=i+1
			end
			Result:=sum
		end

	first_throw_pins:INTEGER
		do
			Result:=throws.item (1)

		end

	is_strike:BOOLEAN
		do
			Result:=FALSE

		end

	is_spare:BOOLEAN
		do
			Result:=FALSE
		end

	last_throw_pins:INTEGER
		do
			Result:=throws.item (3)
		end

	second_throw_pins:INTEGER
		do
			Result:=throws.item (2)

		end

end
