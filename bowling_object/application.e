note
	description: "bowling_object application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature --items

	game:GAME

feature {NONE} -- Initialization

	make
			-- Run application.
		do
			--| Add your code here
			print ("Hello Bowling World!%N")
			create game.make

			game.roll (10)
			game.roll (8)
			game.roll (1)
			print(game.score)
		end

end
